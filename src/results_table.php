<?php require __DIR__ . '/vendor/autoload.php';


use App\Search\Types\SearchBankAccounts;
use App\Search\Highlight\Highlight;
use App\Search\SearchMultiQuery;
use App\Search\SearchRange;
use Elasticsearch\ClientBuilder;
use App\Search\Pagination\Pagination;
use App\Utils\PaginatorUrl;
use App\Search\SearchWildcard;

$search         = isset($_GET['search']) ? $_GET['search'] : '';
$fromBalance    = isset($_GET['fromBalance']) ? $_GET['fromBalance'] : '';
$toBalance      = isset($_GET['toBalance']) ? $_GET['toBalance'] : '';
$fromDate       = isset($_GET['fromDate']) ? $_GET['fromDate'] : '';
$toDate         = isset($_GET['toDate']) ? $_GET['toDate'] : '';
$city           = isset($_GET['city']) ? $_GET['city'] : '';
$state          = isset($_GET['state']) ? $_GET['state'] : '';
$page           = isset($_GET['page']) ? intval($_GET['page']) : 0;


$searchable_fields = ['firstname', 'lastname'];
$highlighted_fields = ['firstname', 'lastname', 'city', 'state'];

$tz  = new DateTimeZone('Europe/Brussels');
if ($fromDate != '') {
    $toAge = DateTime::createFromFormat('d/m/Y', $fromDate, $tz)
        ->diff(new DateTime('now', $tz))
        ->y;
}
if ($toDate != '') {
    $fromAge = DateTime::createFromFormat('d/m/Y', $toDate, $tz)
        ->diff(new DateTime('now', $tz))
        ->y;
}

$searchQuery        = new SearchMultiQuery($search, $searchable_fields);
$searchBalanceRange = new SearchRange('balance', $fromBalance, $toBalance);
$searchAgeRange     = new SearchRange('age', $fromAge, $toAge);
$searchCity         = new SearchWildcard($city, 'city');
$searchState        = new SearchWildcard($state, 'state');
$highlighter        = new Highlight($highlighted_fields);

$client = ClientBuilder::create()
	->setHosts([getenv('ES_HOST')])
	->build();
$searchBankAccounts = new SearchBankAccounts($client, $searchQuery, $searchBalanceRange, $searchAgeRange, $searchCity, $searchState);
$searchBankAccounts->setHighlighter($highlighter);

// Descomenta estas líneas si quieres ver la query que se está ejecutando.
// Útil en el caso de que haya un error de sintaxis
// echo "<pre>";
// echo json_encode($searchBankAccounts->getQuery(), JSON_PRETTY_PRINT);
// echo "</pre>";

$paginator = new Pagination($page);
$searchBankAccounts->setPaginator($paginator);
$filterResults = $searchBankAccounts->search();


function getHighlight($result, $field)
{
	if (isset($result['highlight'][$field][0])) {
		return $result['highlight'][$field][0];
	}
	return $result['_source'][$field];
}

?>

<style>
	.table-results em {
		background-color: #FFFF00;
		font-weight: bold;
	}
    .btn-warning {
        margin: 1px 2.5px;
    }
</style>
<div class="box">
	<div class="box-header">
		<h3 class="box-title">Results (hits=<?= $total_hits = $filterResults['hits']['total'] ?>)</h3>

		<div class="box-tools">
			<div class="input-group input-group-sm" style="width: 150px;">
			</div>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table class="table table-hover table-striped table-results">
			<tbody>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Surname</th>
                <th>Age</th>
				<th>Balance</th>
                <th>City</th>
                <th>State</th>
				<th>Email</th>
				<th>ElasticSearch sort score</th>
			</tr>
			<?php

			foreach ($filterResults['hits']['hits'] as $filterResult) {
				$result = $filterResult['_source'];
				?>
				<tr>
					<td>#<?= $result['account_number'] ?></td>
					<td><?= getHighlight($filterResult, 'firstname') ?></td>
					<td><?= getHighlight($filterResult, 'lastname') ?></td>
                    <td><?= $result['age'] ?></td>
					<td>$ <?= $result['balance'] ?></td>
                    <td><?= getHighlight($filterResult, 'city') ?></td>
                    <td><?= getHighlight($filterResult, 'state') ?></td>
					<td><?= $result['email'] ?></td>
					<td><?= $filterResult['_score'] ?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
        <br /><br /><br />

        <?php
        $paginatorUrl = new PaginatorUrl($_GET, $paginator);
        $totalPages = $paginatorUrl->getTotalPaginationLinks($filterResults['hits']['total'], $paginator->getPerPage());
        ?>
        <?php
        if ( $totalPages > 1 ) {
        ?>
        <a class="btn btn-success" href="<?= $paginatorUrl->getPreviousURL() ?>"> &lt;---Previous</a>
        <?php
        for ($i=0; $i < $totalPages; $i++) {
            echo "<a class=\"btn btn-warning\" href=\"" . $paginatorUrl->getPaginationUrl($i) . "\">" . $i . "</a>" ;
        }
        ?>
        <a class="btn btn-success" href="<?= $paginatorUrl->getNextUrl() ?>">Next----&gt;</a>
            <?php
        }
        ?>
		<hr>
		<?php
		echo "<pre>ElasticSearch QUERY \n";
		echo json_encode($searchBankAccounts->getQuery(), JSON_PRETTY_PRINT);
		echo "</pre>";
		echo "<pre>ElasticSearch Response \n";
		echo json_encode($filterResults, JSON_PRETTY_PRINT);
		echo "</pre>";


		?>
	</div>
</div>
