<!DOCTYPE html>
<html>
<head>
	<title>MPWEB Javascript and CSS example</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://almsaeedstudio.com/themes/AdminLTE/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <style>
        .max-width {
            width: 100%;
        }
    </style>
</head>
<body>

<?php

$search         = (isset($_GET['search'])) ? strtolower($_GET['search']) : '';
$fromBalance    = (isset($_GET['fromBalance'])) ? $_GET['fromBalance'] : '';
$toBalance      = (isset($_GET['toBalance'])) ? $_GET['toBalance'] : '';
$fromDate       = (isset($_GET['fromDate'])) ? $_GET['fromDate'] : '';
$toDate         = (isset($_GET['toDate'])) ? $_GET['toDate'] : '';
$city           = (isset($_GET['city'])) ? strtolower($_GET['city']) : '';
$state          = (isset($_GET['state'])) ? strtolower($_GET['state']) : '';
?>

<div class="container">
	<div class="page-header">
		<h1> Bank accounts </h1>
	</div>


	<div class="box box-primary col-xs-12">
		<form action="/" method="get">
			<div class="box-header with-border">
				<h3 class="box-title">Filter</h3>
			</div>
			<div class="box-body">

				<div class="form-group">
					<label for="search">Search by Name or surname</label>
					<input type="text" class="form-control" name="search" id="search" placeholder="Search"
						   value="<?= $search ?>">
				</div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6" >
                            <label for="city">Search City</label>
                            <input type="text" class="form-control" name="city" id="city" placeholder="Search City"
                                   value="<?= $city ?>">
                        </div>
                        <div class="col-xs-6" >
                            <label for="state">Search State</label>
                            <input type="text" class="form-control" name="state" id="state" placeholder="Search State"
                                   value="<?= $state ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6" >
                            <label for="fromBalance">From balance</label>
                            <input type="number" class="form-control" name="fromBalance" id="fromBalance" placeholder="0"
                                   value="<?= $fromBalance ?>">
                        </div>
                        <div class="col-xs-6" >
                            <label for="toBalance">To balance</label>
                            <input type="number" class="form-control" name="toBalance" id="toBalance" placeholder="999999"
                                   value="<?= $toBalance ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6" >
                            <div class="input-group date max-width" data-date-format="dd/mm/yyyy">
                                <label for="fromDate">From date</label>
                                <input type="text" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/aaaa"
                                       value="<?= $fromDate ?>">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="input-group date max-width" data-date-format="dd/mm/yyyy">
                                <label for="toDate">To date</label>
                                <input type="text" class="form-control" name="toDate" id="toDate" placeholder="dd/mm/aaaa"
                                       value="<?= $toDate ?>">
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Filter</button>
			</div>
		</form>
	</div>

	<div class="col-xs-12">
		<?php include('results_table.php') ?>
	</div>
</div>
<script>
    $('.input-group.date').datepicker({format: "dd/mm/yyyy"});
</script>
</body>
</html>
