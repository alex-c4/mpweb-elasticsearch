<?php namespace App\Search;

use App\Search\Highlight\Highlight;
use App\Search\Pagination\PaginatorInterface;
use Elasticsearch\Client;


abstract class Search
{
	/** @var Client */
	private $client;
	/** @var SearchInterface */
	private $filterQuery;

    /** @var SearchInterface */
    private $filterBalanceRange;

    /** @var SearchInterface */
    private $filterAgeRange;

    /** @var SearchInterface */
    private $filterCity;

    /** @var SearchInterface */
    private $filterState;

	/** @var Highlight */
	private $highlighter;

    /** @var PaginatorInterface */
	private $paginator;

	protected abstract function getIndex();

	protected abstract function getType();

    /**
     * Search constructor.
     * @param Client $client
     * @param SearchInterface $filterQuery
     * @param SearchInterface $filterBalanceRange
     * @param SearchInterface $filterAgeRange
     * @param SearchInterface $filterCity
     * @param SearchInterface $filterState
     */
	public function __construct(Client $client, SearchInterface $filterQuery, SearchInterface $filterBalanceRange, SearchInterface $filterAgeRange, SearchInterface $filterCity, SearchInterface $filterState)
	{
		$this->client               = $client;
		$this->filterQuery          = $filterQuery;
		$this->filterBalanceRange   = $filterBalanceRange;
        $this->filterAgeRange       = $filterAgeRange;
        $this->filterCity           = $filterCity;
        $this->filterState          = $filterState;
	}

    /**
     *
     * @param PaginatorInterface $paginator
     */
	public function setPaginator(PaginatorInterface $paginator)
	{
		$this->paginator = $paginator;
	}

	public function setHighlighter(Highlight $highlighter)
	{
		$this->highlighter = $highlighter;
	}

	public function getQuery()
	{
        $params = [
            'index' => $this->getIndex(),
            'type'  => $this->getType(),
            'body' => [
                'query' => [
                    'bool' => [
                        'must'  => [
                            $this->filterQuery->getFilter(),
                            $this->filterBalanceRange->getFilter(),
                            $this->filterAgeRange->getFilter()
                        ]
                    ]
                ],
                'size'   => $this->paginator->getPerPage(),
                'from'   => $this->paginator->getPage() * $this->paginator->getPerPage(),
            ]
        ];

        if ($this->filterCity->getFilter() !== NULL) {
            $params['body']['query']['bool']['must'][] = $this->filterCity->getFilter();
        }

        if ($this->filterState->getFilter() !== NULL) {
            $params['body']['query']['bool']['must'][] = $this->filterState->getFilter();
        }

        // echo "<pre>";
        // echo json_encode($params, JSON_PRETTY_PRINT);
        // echo "</pre>";
        // die();

		if (isset($this->highlighter)) {
			$params['body'] = array_merge($params['body'], $this->highlighter->highlight());
		}

		return $params;
	}

	public function search()
	{
		$query = $this->getQuery();
		return $this->client->search($query);
	}
}