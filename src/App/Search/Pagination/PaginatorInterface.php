<?php

namespace App\Search\Pagination;


interface PaginatorInterface
{

    public function getPage();

	public function getPerPage();

}