<?php


namespace App\Search\Pagination;


class Pagination implements PaginatorInterface
{
    private $page = 0;
    private $per_page = 10;

    /**
    +	 * Pagination constructor.
    +	 * @param int $page
    +	 * @param int $per_page
    +	 */
	public function __construct($page, $per_page = 10)
	{
		$this->page     = $page;
		$this->per_page = $per_page;
	}

	/**
	 * @return int
	 */
	public function getPage()
	{
        return $this->page;
	}

	/**
	 * @return int
	 */
	public function getPerPage()
	{
        return $this->per_page;
	}
}