<?php


namespace App\Search;


class SearchWildcard implements SearchInterface
{
	/** @var string */
	private $search_text;
	/** @var string */
	private $field;

    /**
     * SearchMultiQuery constructor.
     * @param string $search_text
     * @param string $field
     */
	public function __construct($search_text, string $field)
	{
		$this->search_text = $search_text;
		$this->field       = $field;
	}


	public function getFilter()
	{

        if (empty($this->search_text)) {
            return NULL;
        }

		$result = ["wildcard" => []];

        $result["wildcard"][$this->field]  = ['value' => '*'.$this->search_text.'*'];

		return $result;
	}
}