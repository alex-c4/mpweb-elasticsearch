<?php namespace App\Search;


class SearchRange implements SearchInterface
{

    /** @var array */
    private $range = [];

    /** @var string */
    private $field;

	/**
	 * SearchQuery constructor.
	 * @param int $from
     * @param int $to
	 */
	public function __construct(string $field, $from, $to)
	{
	    $this->field = $field;
		$this->from($from);
		$this->to($to);
	}


    /**
     * @param integer $from
     */
    public function from($from)
    {
        if (!empty($from)) {
            $this->range['gte'] = $from;
        }

        if (empty($from)) {
            $this->range['gte'] = 0;
        }

        return $this;
    }

    /**
     * @param integer $to
     */
    public function to($to)
    {
        if (!empty($to)) {
            $this->range['lte'] = $to;
        }

        return $this;
    }

	public function getFilter()
	{
		return [
            'range'    => [
                $this->field => [ $this->range ]
            ]
		];
	}
}