<?php


namespace App\Utils;


use App\Search\Pagination\PaginatorInterface;

class PaginatorUrl
{
    /** @var array */
    private $queryString = [];

	/** @var  PaginatorInterface */
	private $paginator;

	/**
	 * PaginatorUrl constructor.
	 * @param array $queryString
	 * @param PaginatorInterface $paginator
	 */
	public function __construct(array $queryString, PaginatorInterface $paginator)
	{
        		$this->queryString = $queryString;
        		$this->paginator   = $paginator;
        	}

	public function getPreviousURL()
	{
	    if ($this->paginator->getPage() == 0) {
	        return "";
		}

		$qs         = $this->queryString;
		$qs['page'] = $this->paginator->getPage() - 1;

		return '?' . http_build_query($qs);
	}

	public function getNextUrl()
	{
        		$qs         = $this->queryString;
        		$qs['page'] = $this->paginator->getPage() + 1;

        		return '?' . http_build_query($qs);
	}

	public function getTotalPaginationLinks(int $totalResults, int $perPage): int
    {
        if ($perPage === 0) {
            return 1;
        }

        return ceil($totalResults / $perPage);
    }

    public function getPaginationUrl(int $i)
    {
        $qs         = $this->queryString;
        $qs['page'] = $i;

        return '?' . http_build_query($qs);
    }

}